const root = document.documentElement // html
const rootSyles = getComputedStyle(root)
console.log("rootSyles", rootSyles.getPropertyValue('--bg'))

/**
 * getComputedSytle => CSSOM equivalente al DOM pero para css
 */
/*


const rootStyle = document.documentElement.style

const getScrollBarWidth = () => innerWidth - document.documentElement.clientWidth

rootStyle.setProperty('--scroll-bar-width',getScrollBarWidth())




*/
// manipular variables css con javascript

// root
// document.documentElement
// document.documentElement.style.setProperty('--color', e.target.value)
// const rootSyles = getComputedStyle(root)
// rootSyles.getPropertyValue('--bg')

// otra forma

// const root = document.querySelector(':root');
// const rootStyles = getComputedStyle(root);
// const pink_color = rootStyles.getPropertyValue('--pink');
// root.style.setProperty('--pink', '#F7F00C')


// window.getComputedStyle(document.querySelector('.banner-container')) // CSSOM

/*

document.body.classList.add('name-style')

let size = 2 // dinamic
const styles = `
 background : ${color};
 border-bottom: ${size * 2}px solid black;
`
// lo que hara esto es reemplazar los estilos inline existentes, es decir esto seria como un reset
element.setAttribute('style',styles)

// lo que hara es agremar mas estilos inline existentes, esto seria como un append
element.style += `;${styles}`


*/


